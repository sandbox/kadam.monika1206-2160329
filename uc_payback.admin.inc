<?php

/**
 * @file
 * Administration forms for the uc_payback.
 */

/**
 * Menu callback; Displays the administration settings for uc_payback.
 */
function uc_payback_admin_settings() {
  $form = array();

  $form['uc_payback'] = array(
    '#type' => 'fieldset',
    '#title' => t('UC Payback Settings'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );

  $form['uc_payback']['uc_payback_lmid'] = array(
    '#type' => 'textfield',
    '#title' => t('LMID'),
    '#default_value' => variable_get('uc_payback_lmid', ''),
    '#required' => TRUE,
  );

  $form['uc_payback']['uc_payback_auth_key'] = array(
    '#type' => 'textfield',
    '#title' => t('AUTH KEY'),
    '#default_value' => variable_get('uc_payback_auth_key', ''),
    '#required' => TRUE,
  );
  return system_settings_form($form);
}

/**
 * Validate function for settings form.
 */
function uc_payback_admin_settings_validate($form, $form_state) {

  if ($form_state['values']['uc_payback_lmid'] == '') {
    form_set_error('uc_payback_lmid', t('Please Enter a LMID'));
  }

  if ($form_state['values']['uc_payback_auth_key'] == '') {
    form_set_error('uc_payback_auth_key', t('Please Enter a AUTH KEY'));
  }
}
