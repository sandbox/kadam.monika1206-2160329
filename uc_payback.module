<?php

/**
 * @file
 * Performs payback redemption for ubercart used for UC payback.
 */

/**
 * Impelements hook_permission().
 */
function uc_payback_permission() {
  return array(
    'access payback burn' => array(
      'title' => t('access payback burn'),
      'description' => t('access payback burn'),
    ),
  );
}

/**
 * Impelements hook_menu().
 */
function uc_payback_menu() {

  $items['admin/ubercart/config/payment-methods/manage/uc_payback'] = array(
    'title' => 'Payback',
    'description' => 'Provides configuration options for the Payback.',
    'access arguments' => array('administer payback'),
    'page callback' => 'drupal_get_form',
    'page arguments' => array('uc_payback_admin_settings'),
    'file' => 'uc_payback.admin.inc',
  );

  $items['cart/payback/%'] = array(
    'title' => 'Payback',
    'page callback' => 'uc_payback_back_url',
    'page arguments' => array(2),
    'access arguments' => array('access payback'),
    'type' => MENU_CALLBACK,
  );
  $items['cart/payback/redirect/%'] = array(
    'title' => 'Payback redirect',
    'page callback' => 'uc_payback_burn',
    'page arguments' => array(3),
    'access arguments' => array('access payback'),
    'type' => MENU_CALLBACK,
  );
  return $items;
}

/**
 * Implements hook_block_info().
 */
function uc_payback_block_info() {
  $blocks['Payback'] = array(
    'info' => t('Payback'),
    'weight' => 1,
  );
  return $blocks;
}

/**
 * Implements hook_block_view().
 */
function uc_payback_block_view($delta = '') {
  $block = array();
  switch ($delta) {
    case 'Payback':
      $block['subject'] = t('Redeem Your Payback points here');
      $block['content'] = drupal_get_form('uc_payback_pay_form');
      break;
  }
  return $block;
}

/**
 * Implements Payback redeemption form().
 */
function uc_payback_pay_form($form, &$form_state) {
  drupal_add_js(drupal_get_path('module', 'uc_payback') . '/uc_payback.js', 'file');
  $payback_no = "";
  $order_id = intval($_SESSION['cart_order']);
  $order = uc_order_load($order_id);
  $redeem_pts_max = $order->order_total * 4;
  $form = array();
  $form['order_id'] = array(
    '#type' => 'hidden',
    '#value' => $order_id,
  );
  $form['order_total'] = array(
    '#type' => 'hidden',
    '#prefix' => '<div id = "order_total_val">',
    '#value' => $order->order_total,
    '#suffix' => '</div>',
  );
  $form['payback_form']['payback_msg'] = array(
    '#type' => 'markup',
    '#weight' => -10,
    '#prefix' => '<div class = "max_payback_redeem">1 Payback point = Rs 0.25.<br/>Max points allowed for current order <span>' . $redeem_pts_max . '</span>.<div><div id = "payback_msg1">',
    '#suffix' => '</div>',
  );
  $form['payback_form']['payback_no'] = array(
    '#type' => 'textfield',
    '#prefix' => '<div id = "payback_text">',
    '#title' => t('Enter your Payback number:'),
    '#default_value' => $payback_no,
    '#suffix' => '</div>',
    '#size' => 21,
  );
  $form['payback_form']['payback_value'] = array(
    '#type' => 'textfield',
    '#prefix' => '<div id = "payback_amt">',
    '#title' => t('Enter your points:'),
    '#default_value' => '',
    '#suffix' => '</div>',
    '#size' => 21,
  );
  $form['payback_form']['payback_sub'] = array(
    '#type' => 'markup',
    '#weight' => 10,
    '#prefix' => '<div><div id = "payback_sub">You will be directed to Payback Gateway and <br/> you will require to put your Payback Account PIN.',
    '#suffix' => '</div>',
  );
  $form['#method'] = 'post';
  $form['payback_form']['payback_submit'] = array(
    '#type' => 'submit',
    '#prefix' => '<div id = "payback_submit">',
    '#value' => t('Redeem'),
    '#suffix' => '</div>',
  );
  return $form;
}

/**
 * Validate function for settings form.
 */
function uc_payback_admin_settings_validate($form, $form_state) {
  $lmid = variable_get('uc_payback_lmid');
  $auth_key = variable_get('uc_payback_auth_key');

  if (trim($lmid) == '' || trim($auth_key) == '') {
    form_set_error('uc_payback_lmid', t('You are not allowed to Redeem payback. Please contact site administrator.'));
  }
}

/**
 * Implements Payback redeemption form submit (). 
 */
function uc_payback_pay_form_submit($form, &$form_state) {
  $order_id = intval($_SESSION['cart_order']);
  $payback = $form_state['input']['payback_no'];
  $amount = $form_state['input']['payback_value'];
  global $user;
  global $base_url;
  $payback_amt = $amount;
  $digest = variable_get('uc_payback_auth_key');
  $lmid = variable_get('uc_payback_lmid');
  db_delete('uc_payback_burn')
      ->condition('order_id', $order_id)
      ->condition('user_id', $user->uid)
      ->execute();
  if ($user->uid > 0) {
    foreach ($user->roles as $key => $role) {
      if ($key != 2) {
        $type = $role;
      }
    }
  }
  else {
    $type = "GUEST";
  }
  db_insert('uc_payback_burn')
      ->fields(array(
        'user_id',
        'order_id',
        'payback_no',
        'payback_amt',
        'user_type',
        'created',
      ))
      ->values(array(
        'user_id' => $user->uid,
        'order_id' => $order_id,
        'payback_no' => $payback,
        'payback_amt' => $payback_amt,
        'user_type' => $type,
        'created' => REQUEST_TIME,
      ))
      ->execute();
  $data = "lmid=$lmid&loyaltyNo=$payback&trnsid=$order_id&points=$payback_amt&digest=$digest&backUrl=$base_url/cart/payback/$order_id&redirectUrl=$base_url/cart/payback/redirect/$order_id";
  $options = array(
    'method' => 'POST',
    'data' => $data,
    'timeout' => 15,
    'headers' => array('Content-Type' => 'application/x-www-form-urlencoded'),
  );
  $result = drupal_http_request('https://npg.payback.in/nextgen-pgateway/NewPointGateway', $options);
  watchdog('payback url', 'payback at URL <pre> @order_id.</pre>', array('@order_id' => print_r($result, TRUE)));
  $data = $result->data;
  watchdog('payback data', 'payback data <pre> @order_id.</pre>', array('@order_id' => print_r($data, TRUE)));
  drupal_goto($data);
}

/**
 * Implements payback redirect url.
 */
function uc_payback_burn($order_id) {
  $order = uc_order_load($order_id);
  watchdog('order payback', 'Receiving payback at URL for order <pre> @order_id.</pre>', array('@order_id' => print_r($_REQUEST, TRUE)));
  $redeemedpts = $return_order_id = $transactionstatus = $payback_orderid = '';
  if (isset($_GET['transactionId'])) {
    if (isset($_REQUEST['transactionId'])) {
      $return_order_id = $_REQUEST['transactionId'];
    }
    if (isset($_REQUEST['txnDigest'])) {
      $txndigest = $_REQUEST['txnDigest'];
    }if (isset($_REQUEST['transactionStatus'])) {
      $transactionstatus = $_REQUEST['transactionStatus'];
    }if (isset($_REQUEST['orderid'])) {
      $payback_orderid = $_REQUEST['orderid'];
    }if (isset($_REQUEST['redeemedPts'])) {
      $redeemedpts = $_REQUEST['redeemedPts'];
    }
    if ($order_id == $return_order_id) {
      $query = db_select('uc_payback_burn', 'uc');
      $query->condition('uc.user_id', $order->uid);
      $query->condition('uc.order_id', $order->order_id);
      $query->fields('uc');
      $payback_fetch = $query->execute()->fetchObject();
    }
    if (isset($payback_fetch)) {
      db_insert('uc_payback_burn_response')
          ->fields(array(
            'user_id',
            'order_id',
            'payback_no',
            'txnDigest',
            'redeemedPts',
            'transactionStatus',
            'payback_orderid',
            'created',
          ))
          ->values(array(
            'user_id' => $order->uid,
            'order_id' => $order_id,
            'payback_no' => $payback_fetch->payback_no,
            'txnDigest' => $txndigest,
            'redeemedPts' => $redeemedpts,
            'transactionStatus' => $transactionstatus,
            'payback_orderid' => $payback_orderid,
            'created' => REQUEST_TIME,
          ))
          ->execute();
    }
    if ($transactionstatus == 'Approved' && $payback_fetch->payback_amt == $redeemedpts) {
      $type = 'payback_points';
      $title = 'Payback Points';
      $paid = $redeemedpts * 0.25;
      $amt = -($redeemedpts * 0.25);
      $order_bal_amt = $order->order_total - $paid;
      drupal_set_message(t('You have successfully made payment using payback 
        points : @redeemedpts. Amount paid from payback points is @paid Rs.(4 points = 1 rupee).', array('@redeemedpts' => $redeemedpts, '@paid' => $paid)));
      if ($order_bal_amt > 0) {
        drupal_set_message(t('Please select below payment option for 
          remaining order amount : @order_bal_amt Rs.', array('@order_bal_amt' => $order_bal_amt)));
      }
      uc_order_line_item_add($order->order_id, $type, $title, $amt, NULL, NULL);
    }
  }
  else {
    drupal_set_message(t("You order failed at payback ."));
  }
  drupal_goto('cart/checkout');
}

/**
 * Implements payback back url.
 */
function uc_payback_back_url($order_id) {
  $order = uc_order_load($order_id);
  if ($order) {
    $query = db_select('uc_payback_burn', 'uc');
    $query->condition('uc.user_id', $order->uid);
    $query->condition('uc.order_id', $order->order_id);
    $query->fields('uc');
    $payback_fetch = $query->execute()->fetchObject();
    if (isset($_GET['error'])) {
      $status = $_GET['error'];
      drupal_set_message(t('Sorry!!! Your payment was failed using payback points.'));
      $order = uc_order_load($order_id);
      if ($order) {
        $query = db_select('uc_payback_burn', 'uc');
        $query->condition('uc.user_id', $order->uid);
        $query->condition('uc.order_id', $order->order_id);
        $query->fields('uc');
        $payback_fetch = $query->execute()->fetchObject();
        $status = $_GET['error'];
      }
    }
    else {
      $status = "User selected cancel option at payback";
      drupal_set_message(t("You cancelled payback option from payback gateway @order_id.", array('@order_id' => $order_id)));
    }
    if (isset($payback_fetch)) {
      db_insert('uc_payback_burn_response')
          ->fields(array(
            'user_id',
            'order_id',
            'payback_no',
            'redeemedPts',
            'transactionStatus',
            'created',
          ))
          ->values(array(
            'user_id' => $order->uid,
            'order_id' => $order_id,
            'payback_no' => $payback_fetch->payback_no,
            'redeemedPts' => $payback_fetch->payback_amt,
            'transactionStatus' => $status,
            'created' => REQUEST_TIME,
          ))
          ->execute();
    }
    drupal_goto('cart/checkout');
  }
  else {
    drupal_set_message(t("Sorry!!! The order does not exist!!!"));
    drupal_goto('cart');
  }
}

/**
 * Implements hook_order().
 */
function uc_payback_uc_order($op, $order, $arg2) {
  switch ($op) {
    case 'load':
      if ($order->order_status == 'in_checkout' || $arg2 == 'in_checkout' || $order->order_status == 'payment_received' || $arg2 == 'payment_received' || $order->order_status == 'abandoned' || $arg2 == 'abandoned') {
        if (isset($order->payment_method)) {
          $query = db_select('uc_payback_burn', 'uc');
          $query->condition('uc.user_id', $order->uid);
          $query->condition('uc.order_id', $order->order_id);
          $query->fields('uc', array('payback_no', 'payback_amt'));
          $payback_no = $query->execute()->fetchObject();

          if ($payback_no) {
            watchdog('payback_amt', 'goes here !payback_no !payback_amt', array('!payback_no' => $payback_no->payback_no, '!payback_amt' => $payback_no->payback_amt));
            $order->payback_burn_no = $payback_no->payback_no;
            $order->payback_burn_points = $payback_no->payback_amt;
            $order->payback_burn_amount = $payback_no->payback_amt * 0.25;
          }
        }
      }
      break;
  }
}

/**
 * Implements hook_uc_line_item().
 */
function uc_payback_uc_line_item() {
  $items[] = array(
    'id' => 'payback_points',
    'title' => t('Payback Points'),
    'weight' => 2,
    'default' => FALSE,
    'stored' => TRUE,
    'add_list' => TRUE,
    'calculated' => TRUE,
  );
  return $items;
}
