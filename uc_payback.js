/**
 * @file
 * Administration forms for the uc_payback.
 */

(function($) {
    $(document).ready(function() {
        document.getElementById("edit-payback-submit").disabled = true;
        /*  edit-payback-submit  */
        $("#edit-payback-submit").click(function() {
            var pay_no = $("#edit-payback-no").val();
            var pay_atm = $("#edit-payback-value").val();
            document.getElementById('payback_msg1').innerHTML = '';
            if (!pay_no) {
                document.getElementById("payback_msg1").innerHTML = "<b><div style='color: #FF6633;'>Enter a valid 16 digit Payback number[0-9].</div></b>";
                return false;
            }
            if (!pay_atm) {
                document.getElementById("payback_msg1").innerHTML = "<b><div style='color: #FF6633;'>Enter valid payback points[0-9].</div></b>";
                return false;
            }
        });
        /* payback Amount validation  */
        $("#edit-payback-no").bind('input', function() {
            var payback_no = $("#edit-payback-no").val();
            if (!isInteger(payback_no)) {
                document.getElementById("payback_msg1").innerHTML = "<b><div style='color: #FF6633;'>Enter a valid 16 digit Payback number[0-9].</div></b>";
                document.getElementById("edit-payback-submit").disabled = true;
            }
            else if (payback_no.length < 16 || payback_no.length > 16) {
                document.getElementById("payback_msg1").innerHTML = "<b><div style='color: #FF6633;'>Payback number should be 16 digit[0-9].</div></b>";
                document.getElementById("edit-payback-submit").disabled = true;
            }
            else {
                document.getElementById('edit-payback-no').value = payback_no;
                document.getElementById('payback_msg1').innerHTML = '';
            }
        });

        /* payback Amount validation  */
        $('#edit-payback-value').bind('input', function() {
            var payback_amt = parseInt($('#edit-payback-value').val());
            var redeem_pts_max = parseInt($('.max_payback_redeem span').html());
            if (!isInteger(payback_amt)) {
                document.getElementById("payback_msg1").innerHTML = "<b><div style='color: #FF6633;'>Enter valid payback points[0-9].</div></b>";
                document.getElementById("edit-payback-submit").disabled = true;
            }
            else if (payback_amt == 0) {
                document.getElementById("payback_msg1").innerHTML = "<b><div style='color: #FF6633;'>Payback points should be greater than zero[0].</div></b>";
                document.getElementById("edit-payback-submit").disabled = true;
            } else if (redeem_pts_max <= payback_amt) {
                document.getElementById("payback_msg1").innerHTML = "<b><div style='color: #FF6633;'>Payback points should be less than " + redeem_pts_max + ".</div></b>";
                document.getElementById("edit-payback-submit").disabled = true;
            }
            else {
                var payback_no = $("#edit-payback-no").val();
                if (!isInteger(payback_no)) {
                    document.getElementById("payback_msg1").innerHTML = "<b><div style='color: #FF6633;'>Enter a valid 16 digit Payback number[0-9].</div></b>";
                    document.getElementById("edit-payback-submit").disabled = true;
                }
                else if (payback_no.length < 16 || payback_no.length > 16) {
                    document.getElementById("payback_msg1").innerHTML = "<b><div style='color: #FF6633;'>Payback number should be 16 digit[0-9].</div></b>";
                    document.getElementById("edit-payback-submit").disabled = true;
                } else {
                    document.getElementById("edit-payback-value").val = payback_amt;
                    document.getElementById('payback_msg1').innerHTML = '';
                    document.getElementById("edit-payback-submit").disabled = false;
                }
            }
        });
        var isInteger_re = /^\s*?\d+\s*$/;
        function isInteger(s) {
            return String(s).search(isInteger_re) != -1;
        }
    });
})
(jQuery);
